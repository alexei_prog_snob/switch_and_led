
#define GPIO_PORTE_DIR_R        (*((volatile unsigned long *)0x40024400))
#define GPIO_PORTE_AFSEL_R      (*((volatile unsigned long *)0x40024420))
#define GPIO_PORTE_PUR_R        (*((volatile unsigned long *)0x40024510))
#define GPIO_PORTE_DEN_R        (*((volatile unsigned long *)0x4002451C))
#define GPIO_PORTE_LOCK_R       (*((volatile unsigned long *)0x40024520))
#define GPIO_PORTE_CR_R         (*((volatile unsigned long *)0x40024524))
#define GPIO_PORTE_AMSEL_R      (*((volatile unsigned long *)0x40024528))
#define GPIO_PORTE_PCTL_R       (*((volatile unsigned long *)0x4002452C))

#define SYSCTL_RCGC2_R          (*((volatile unsigned long *)0x400FE108))

#define SW1 (*((volatile unsigned long *)0x40024004))
#define OUT (*((volatile unsigned long *)0x40024008))

// FUNCTION PROTOTYPES: Each subroutine defined
void PortE_Init(void);
void Delay(unsigned long msec);
// ***** 3. Subroutines Section *****

// PE0, PB0, or PA2 connected to positive logic momentary switch using 10k ohm pull down resistor
// PE1, PB1, or PA3 connected to positive logic LED through 470 ohm current limiting resistor
// To avoid damaging your hardware, ensure that your circuits match the schematic
// shown in Lab8_artist.sch (PCB Artist schematic file) or
// Lab8_artist.pdf (compatible with many various readers like Adobe Acrobat).
int main(void){
    unsigned long count = 100;
    PortE_Init();

    while(1){
        OUT = 0x02;
        Delay(100);
        while(SW1 != 0x00 && count != 0){
            OUT = 0x00;
            --count;
            Delay(1);
        }
        count = 100;
    }

}

void Delay(unsigned long msec){
    unsigned long volatile time;
    time = 727240*msec/91;  // 0.1sec
    while(time){
        time--;
    }
}

void PortE_Init(void){
    volatile unsigned long delay;
    SYSCTL_RCGC2_R |= 0x00000010;     // 1) F clock
    delay = SYSCTL_RCGC2_R;           // delay
    GPIO_PORTE_LOCK_R = 0x4C4F434B;   // 2) unlock PortE
    GPIO_PORTE_CR_R = 0x1F;           // allow changes to PF4-0
    GPIO_PORTE_AMSEL_R = 0x00;        // 3) disable analog function
    GPIO_PORTE_PCTL_R = 0x00000000;   // 4) GPIO clear bit PCTL
    GPIO_PORTE_DIR_R = 0x02;          // 5) PE0 input, PF1 output
    GPIO_PORTE_AFSEL_R = 0x00;        // 6) no alternate function
    GPIO_PORTE_PUR_R = 0x00;          // enable pullup resistors on PE0
    GPIO_PORTE_DEN_R = 0x03;          // 7) enable digital pins PE1-PF0
}
